# General

Basis of Integral Calculus is to find the **area** underneath a curve. Divide the graph into
"subdivisions" of $\Delta x$ => with the more $\Delta x$, the more accurate we will get as we sum then.

$$lim_{n \to \infty} \sum_{i=1}^{n}f(x_i)\Delta x_i$$

Or in basic words, the limits of [[Sigma sum ]] an infinitely small subdivisions.

This would translate to calculus as follows: $\int_{a}^{b} f(x) \, dx$.

## Explanation

$\int_{a}^{b}$ is the integral, ie. specifying that we do this from a -> b.

$f(x)$ is the curve above the area.

$dx$ is for the infinitely small subdivisions. Could also think as a **full-stop**.

# Antiderivatives

f -> f', derivatives

f' -> f, antiderivatives, or the **integrals**

# Visualization

## Real World Example

> Tank is being filled at a rate of 5L/min for 6 min.
> What is its volume?

$$
\begin{split}
V &= T \cdot R \\
  &= 6min \cdot 5L/min \\
  &= 30L
\end{split}
$$

Graph $r_1(t) = 5$, area of $t = 0 \to 6$ is equal to the volume, $V$.

> Tank is being filled at a rate of $r_2(t) = 6sin(0.3t)$ for 6t
> What is its volume?

Create subdivisions with infinitely smaller sizes, then using [[Riemann Sum]]  to [calculate](https://www.desmos.com/calculator/ld919mfo4c):
$\int_{0}^{6} r_2(t) , dt \approx 24.5L$

**NOTE**: The definite integral is ***net change***, not total!

## Common Mistakes

Ignoring initial conditions. The interpretation will be wrong because again, integral is net change.

Don't forget the **lower bound**!

# Rates of Changes

|                       | Quantity | Rate  |
|-----------------------|----------|-------|
| Differential calculus | $f(x)$   | $f'(x)$ |
| Integral Calculus     | $F(x)= \int_{a}^{x}f(x),dx$ [^1] | $f(x)$  |

GOAL of *differential* calculus: Give a quantity at a point, find its' **instantaneous rate**

GOAL of *integral* calculus: Give a rate, find its' **accumulated quantity**

[Desmos link for visualization](https://www.desmos.com/calculator/k4vlf1nzgh)

## Example

> The function (k(t)) gives the amount of ketchup (in kilograms) produced in a sauce factory by time (t) (hours) on a given day.

$\int_{0}^{4} k'(t) \, dt$ represents the amount of ketchup produced in the first 4 hours

**Rationale**: $k(t)$ gives us quantity of ketchup => $k'(t)$ gives us production rate => integral of production rate gives us quantity from hour 0 -> 4.

[^1]: F(x) is the antiderivative
