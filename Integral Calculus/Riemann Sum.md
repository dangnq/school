An approximation method for calculating area underneath a curve

Useful [link](https://www.desmos.com/calculator/wdhaekerp8) for visualization

Gist of it is: **RECTANGLES**

Given $f(x) = x^2+1$, calculate area between x 1->3

We can divide into subdivisions of $\Delta x$

Ex: $\Delta x = \frac{31}{4} = \frac{1}{2}$, so our precision is 4 subdivisions

$$
\begin{aligned}
\text{Area} &=f(1) \Delta x + f(1.5) \Delta x + f(2) \Delta x + f(2.5)
\Delta x \\
&= 2 \cdot \frac{1}{2} + 3.25 \cdot \frac{1}{2} + 5 \cdot \frac{1}{2} +
7.25 \cdot \frac{1}{2} \\
&= 8.75
\end{aligned}
$$

# Left & Right Riemann Sums

There's two way: Left sums and right sums

Left sums touch the curve with their top left corner

Right sums touch the curve with their top right corner

The subdivisions can be *uniform* or *nonuniform* so $\Delta x$ might not be the same

## Over and Underestimation

| **Direction** | **Left Riemann Sum** | **Right Riemann Sum** |
| --------------|----------------------|-----------------------|
| Increasing    | Underestimation      | Overestimation        |
| Decreasing    | Overestimation       | Underestimation       |

## With a Table

The differences in x is the width, $\Delta x$

$f(x)$ is gonna be height

# Midpoint Riemann Sums

Both left and right Riemann not accurate

Midpoint *compromises* that

Create rectangle at the middle of a $\Delta x$

# Trapezoid Riemann Sums

> Give a function of $f(x) = \sqrt{x+1}$.
> Estimate A under 1 -> 6 using trapezoid

6 points => 5 subdivisions => $\Delta x = \frac{6-1}{5} = 1$

$$
\begin{aligned}
\text{Area} &\approx \frac{f(1)+f(2)}{2} \Delta x + \frac{f(2)+f(3)}{2} \Delta x + \frac{f(3)+f(4)}{2} \Delta x \\
& + \frac{f(4)+f(5)}{2} \Delta x + \frac{f(5)+f(6)}{2} \Delta x \\
& \approx 7.26
\end{aligned}
$$

# Generalization with Sigma Sum

We can generalize the summation Riemann triangles using [[Sigma sum]]

>  Calculate are underneath a curve from a -> b using left Riemann sum

Firstly, we want to the width to be uniform: $\Delta x = \frac{b-a}{2}$

We have $x_0$, $x_1$, ... to be the point of each triangle, and that the last point that we need is $x_n-1$ for the $n^{th}$ triangle (since we take the left point)

$$
 
\begin{aligned}
	a &= x_0 \\
	x_1 &= x_0 + \Delta x \\
	x_2 &= x_1 + \Delta x \\
	& \text{...} \\
	b = x_n &= x_{n-1} + \Delta x 
\end{aligned}
$$

So the approximate area should be:

$$
\begin{aligned}
	\text{A} &=  f(x_0)\Delta x + f(x_1)\Delta x + 
	f(x_2)\Delta x + \text {...} + f(x_{n-1})\Delta x \\
	&= \sum_{i=1}^{n} f(x_{n-1})\Delta x
\end{aligned}
$$

## Ways to Do it

> Approx area under graph of $f(x)=\sqrt{x}$ between x=0.5 and x=3.5

We need four right rectangle for Riemann => $\Delta x = 0.75$

Let $A(i)$ = $i^{th}$ rectangle

The height depends on right point: $f(i) = \sqrt{x_i} = \sqrt{0.5+\Delta x i}$

The area: $A(i) = \Delta x \cdot f(i)$

So, the area:

$$
\begin{aligned}
	\text{A} &= \sum_{i=1}^{4} A(i) \\
			 &= \sum_{i=1}^{4} \Delta x \cdot f(i) \\
			 &= \sum_{i=1}^{4} 0.75 \cdot \sqrt{0.5 + 0.75 \cdot i}
\end{aligned}
$$

## Summary

$\Delta x$ is the width of the triangle

a = $x_0$,  b = $x_n$ (does not apply for mid point)
Mid point: $x_0$ = a + $\frac{\Delta x}{2}$

$x_i$ is the right endpoint, and $x_i = a + \Delta x \cdot i$

Area = $\Delta x \cdot f(x_i)$

| Left Riemann sum | Right Riemann Sum|
| ---| ---|
| $$\sum_{i=0}^{n-1} f(x_i)$$ | $$\sum_{i=1}^{n} f(x_i)$$ |
