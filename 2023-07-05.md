$$
\begin{aligned}
\Delta x &= \frac{\pi}{18} \\
x_0 &= \frac{\Delta x}{2} \\
x_i &= x_0 +  i \cdot \Delta x \\
\text{A} &= \frac{\pi}{18} \cdot f(\frac{\pi}{36} + \frac{1}
{18}i) \\
& \sum_{i=0}^{5}\tan(\frac{\pi}{36}+\frac{1}{18}i) \cdot 
\frac{pi}{18}
\end{aligned}
$$

Integrating Sigma sum with Riemann.
TODO: Deploy the notes!
